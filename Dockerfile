# -- See https://nodejs.org/en/docs/guides/nodejs-docker-webapp/
FROM node:8.2
MAINTAINER Vui Le "amelon@gmail.com"

RUN apt-get update && apt-get install -y \
    vim \
    curl \
    jq


ENV PATH /usr/src/app/node_modules/.bin:/usr/src/app/node_modules/riot/node_modules/.bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
ENV NODE_PATH /usr/local/lib/node_modules:/usr/src/app/node_modules

# It is critical that babel-core is installed as a global module for Riot/ES6
# to work properly. I'm not sure why, but just do it...
# See http://riotjs.com/guide/compiler/#pre-processors
RUN npm install -g \
    babel-core \
    pug \
    pug-cli \
    stylus \
    documentation \
    express \
    morgan \
    winston

# Adjust timezone from UTC to PST
RUN mv /etc/localtime /etc/localtime.old \
    && ln -s /usr/share/zoneinfo/America/Los_Angeles /etc/localtime

RUN mkdir -p /code
COPY code/entrypoint_loop.sh /code/

# -- App directory

RUN mkdir -p /usr/src/app
ADD json-server /usr/src/app
RUN cd /usr/src/app; \
    npm install; \
    npm run build

#VOLUME /usr/src/app
WORKDIR /usr/src/app


