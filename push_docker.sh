#!/bin/sh
# When pushing out an image to DockerHub without specifying the tag, it will look for the latest
# and the tag ID that it is associated with.
image=amelon1/json-server
docker push $image

