#!/bin/sh

id=2
image=amelon1/json-server

docker build \
    -t $image:$id \
    -t $image:latest \
    -f Dockerfile \
    .

