### Summary
Create a Docker container to run json-server. This is borrowed from https://hub.docker.com/r/clue/json-server/.

### Usage
* To serve from the JSON data files, put db.json (and routes.json) into data/ directory. If you use other names for the data files, change then in docker-compose.yml.
* To build it:

        % build_docker.sh

* To run it:

        % docker-compose up -d

* To monitor the json-server log:

        % docker-compose logs -f

* Now open the following pages:
    * http://localhost:3000/posts
    * http://localhost:3000/comments
    * http://localhost:3000/db

* To stop it:

        % docker-compose down

### History
* 2017-07-24 Added json-server v0.11.2.


